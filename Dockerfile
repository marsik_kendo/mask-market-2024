FROM openjdk:17-alpine

COPY ./target/market-*.jar /opt/market.jar


ENTRYPOINT java -jar /opt/market.jar

#ENTRYPOINT /bin/shd