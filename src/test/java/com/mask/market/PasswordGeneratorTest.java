package com.mask.market;


import com.mask.market.services.GeneratePassword;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Objects;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("disabled_in_tests")
public class PasswordGeneratorTest {

    @Autowired
    private GeneratePassword generatePassword;

    @Test
    public void passwordGeneratorTest() {

        String pass = generatePassword.generate();

        Assert.assertEquals(12, pass.length());
        Assert.assertFalse(pass.matches(".*(;|,).*"));

    }
}