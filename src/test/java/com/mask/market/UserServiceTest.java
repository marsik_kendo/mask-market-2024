package com.mask.market;


import com.mask.market.entities.User;
import com.mask.market.repositories.UserRepository;
import com.mask.market.services.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = UserService.class)
public class UserServiceTest {

//    @Autowired
//    private UserService userService;

//    @MockBean
//    private UserRepository userRepository;

    @Test
    public void findOneUserTest() {

        UserRepository userRepository = Mockito.mock(UserRepository.class);

        UserService userService = new UserService(userRepository, Mockito.mock(BCryptPasswordEncoder.class));


        User userFromDB = new User();
        userFromDB.setPhone("89380000000");
        userFromDB.setEmail("admin@mail.ru");


        Mockito.doReturn(userFromDB)
                .when(userRepository)
                .findOneByUsername("89380000000");


        User userJohn = userService.getUserByUserName("89380000000");


        Assert.assertNotNull(userJohn);
        Mockito.verify(userRepository, Mockito.times(1)).findOneByUsername(ArgumentMatchers.eq("89380000000"));
//        Mockito.verify(userRepository, Mockito.times(1)).findOneByUsername(ArgumentMatchers.any(String.class));
    }
}
