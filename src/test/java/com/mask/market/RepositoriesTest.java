package com.mask.market;


import com.mask.market.entities.Product;
import com.mask.market.repositories.ProductsRepository;
import com.mask.market.services.ProductsService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
// @TestPropertySource(locations="classpath:test.properties")
public class RepositoriesTest {
    @Autowired
    private ProductsRepository productsRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void productRepositoryTest() {
        Product product = new Product(null, "Bread", 25);
        Product out = entityManager.persist(product);
        entityManager.flush();

        List<Product> productsList = (List<Product>)productsRepository.findAll();
        System.out.println(productsList);

        Assert.assertEquals(1, productsList.size());
    }
}
