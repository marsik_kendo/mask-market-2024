package com.mask.market;


import com.mask.market.controllers.ProductsRestController;
import com.mask.market.entities.Product;
import com.mask.market.repositories.ProductsRepository;
import com.mask.market.services.ProductsService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
//@SpringBootTest(classes = ProductsRestController.class)
@WebMvcTest(controllers = {ProductsRestController.class})
@AutoConfigureMockMvc
@Ignore
public class DisabledProductRestTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductsService productsService;

    @MockBean
    private ProductsRepository productsRepository;

    // https://support.smartbear.com/alertsite/docs/monitors/api/endpoint/jsonpath.html

    @Test
    public void getAllProductsTest() throws Exception {
        List<Product> allProducts = Arrays.asList(
                new Product(1L, "Milk", 90),
                new Product(2L, "Bread", 25),
                new Product(3L, "Cheese", 320)
        );

        given(productsService.findAllProducts()).willReturn(allProducts);

        mvc.perform(get("/api/v1/products")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$.products").exists())
                .andExpect(jsonPath("$.products").isArray())
                .andExpect(jsonPath("$[0].title", is(allProducts.get(0).getTitle())));
    }
}
