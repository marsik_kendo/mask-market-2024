package com.mask.market.rabitmq;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mask.market.model.Cat;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.BindingBuilder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.mask.market.rabitmq.QueueConfiguration.DLX_EXCHANGE_NAME;

@Component
@RequiredArgsConstructor
public class RabbitReceiver {


    private final ObjectMapper objectMapper;

    private final AmqpAdmin amqpAdmin;

    public static final String EXCHANGE_NAME = "my.exchange";

    public static final String QUEUE_NAME = EXCHANGE_NAME + ".myQueue";

    public static final String QUEUE_NAME2 = EXCHANGE_NAME + ".myQueue2";


    public static final String ROUTING_KEY1 = "qwe1";

    public static final String ROUTING_KEY2 = "qwe2";


    @PostConstruct
    public void init() {

        initInQueues();
        initOutQueues();

    }



    private void initOutQueues() {

        // Основной Exchange
        Exchange outExchange = new DirectExchange("out.exchange", true, false);
        amqpAdmin.declareExchange(outExchange);


        initRetryDlxQueue(outExchange, "out.exchange.out.queue", "zxc1");
        initRetryDlxQueue(outExchange, "out.exchange.out.queue2", "zxc2");
    }

    private void initInQueues() {

        // Основной Exchange
        Exchange mainExchange = new DirectExchange(EXCHANGE_NAME, true, false);
        amqpAdmin.declareExchange(mainExchange);


        initRetryDlxQueue(mainExchange, QUEUE_NAME, ROUTING_KEY1);
        initRetryDlxQueue(mainExchange, QUEUE_NAME2, ROUTING_KEY2);
    }

    public void initRetryDlxQueue(Exchange exchange, String queueName, String routingKey) {

        String dlxQueueName = DLX_EXCHANGE_NAME + "." + queueName;
        Map<String, Object> params = new HashMap<>();
        params.put("x-dead-letter-exchange", "DLX");
        params.put("x-dead-letter-routing-key", dlxQueueName);

        Queue queue = new Queue(queueName, true, false, false, params);
        amqpAdmin.declareQueue(queue);
        amqpAdmin
                .declareBinding(BindingBuilder.bind(queue).to(exchange).with(routingKey).noargs());

        Map<String, Object> dlxArgs = new HashMap<>();
        dlxArgs.put("x-dead-letter-exchange", exchange.getName());
        dlxArgs.put("x-message-ttl", TimeUnit.MINUTES.toMillis(30));
        Queue dlxQueue = new Queue(dlxQueueName, true, false, false, dlxArgs);
        amqpAdmin.declareQueue(dlxQueue);

        amqpAdmin.declareBinding(BindingBuilder.bind(dlxQueue).to(QueueConfiguration.DLX)
                .with(dlxQueueName).noargs());
    }


    @RabbitListener(queues = {QUEUE_NAME, QUEUE_NAME2})
    public void receive(Message message) throws IOException {

        process(message);
    }

    public void process(Message message)
            throws JsonProcessingException {

        System.out.println(message.getMessageProperties().getHeader("DOTMsgType").toString());
        String payload = new String(message.getBody(), StandardCharsets.UTF_8);
        Cat cat = objectMapper.readValue(payload, Cat.class);
        System.out.println(cat);
        int a =1;

//        if(a>0){
//            throw new RuntimeException("qweqweqwe");
//        }

    }


}
