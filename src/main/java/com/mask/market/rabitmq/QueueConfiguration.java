package com.mask.market.rabitmq;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Configuration
public class QueueConfiguration {

    public static final String DLX_EXCHANGE_NAME = "DLX";
    private static final String DLX_UNKNOWN_EXCHANGE_NAME = "DLX.unknown";

    public static Exchange DLX;


    @Autowired
    private AmqpAdmin amqpAdmin;


    @PostConstruct
    public void init() {

//        Map<String, Object> params = new HashMap<>();
//        params.put("alternate-exchange", DLX_UNKNOWN_EXCHANGE_NAME);
        DLX = new DirectExchange(DLX_EXCHANGE_NAME, true, false);

        amqpAdmin.declareExchange(DLX);
    }

}
