package com.mask.market.controllers;

import com.mask.market.entities.User;
import com.mask.market.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

@Controller
@RequestMapping("/personal")
@RequiredArgsConstructor
public class PersonalController {
  private  final UserService userService;

    @GetMapping("/profile")
    public String showProfile(){
        User userByUserName = userService.getUserByUserName("admin");

        return "user_profile";
    }

    @GetMapping("/orders")
    public String historyOrders(Principal principal, Model model){

        User user = userService.getUserByUserName(principal.getName());

        model.addAttribute("orders", user.getOrders());

        return "user_history_orders";
    }



}
