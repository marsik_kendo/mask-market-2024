package com.mask.market.controllers;

import com.mask.market.entities.User;
import com.mask.market.repositories.ProductsRepository;
import com.mask.market.services.UserService;
import com.mask.market.services.transactionals.Service1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

@Controller
//@RequiredArgsConstructor
public class MainController {

    @Autowired
    private  UserService userService;

    @Value("qwe")
    private  String url;


    @Autowired
    private Service1 service1;


    @GetMapping ("/qwe")
    public String qwe(BCryptPasswordEncoder encoder, @RequestParam String pass){

        System.out.println(encoder.encode(pass));

        return "profile";
    }

    @GetMapping ("/admin")
    public String admin(){



        return "index";
    }

//    @GetMapping ("/personal/profile")
//    public String profile(Principal principal, Model model){
//
//        System.out.println(url);
//        User user = userService.getUserByUserName(null);
//        int a =1;
//
//
//        System.out.println(user.getRoles());
//        model.addAttribute("user", user);
//        return "profile";
//    }

//    @GetMapping("/personal/changePass/{pass}")
//    public String profile(Principal principal, @PathVariable String pass, PasswordEncoder encoder){
//
//        User user = userService.getUserByUserName(null);
//        user.setPassword(encoder.encode(pass));
//
//
////        model.addAttribute("user", user);
//        return "profile";
//    }


    @GetMapping
    public String index(ProductsRepository repository){

        service1.qwe();
        return "index";
    }

    @GetMapping("/login")
    public String login(){
        return "login";
    }
}
