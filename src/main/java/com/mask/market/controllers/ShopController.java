package com.mask.market.controllers;

import com.mask.market.entities.Product;
import com.mask.market.services.ProductsService;
import com.mask.market.services.UserService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/shop")
@RequiredArgsConstructor
public class ShopController {


    private  final ProductsService productsService;

    @GetMapping
    public String showProducts(Model model,
                               @RequestParam(required = false) String word,
                               @RequestParam(required = false) Integer min,
                               @RequestParam(required = false) Integer max,
                               @RequestParam(required = false, defaultValue = "1") Integer pageNumber){

        PageRequest pageRequest = PageRequest.of(pageNumber - 1, 5, Sort.Direction.DESC, "title");
        Page<Product> page = productsService.findAllProducts(word, min, max , pageRequest);
        String filters = getFilters(word, min, max);


        model.addAttribute("page", page);
        model.addAttribute("filters", filters);

        return "shop";
    }

    private String getFilters(String word, Integer min, Integer max){

        StringBuilder stringBuilder = new StringBuilder();
        if(word != null && !word.isEmpty()){
            stringBuilder.append("&word=" + word);
        }
        if(min != null ){
            stringBuilder.append("&min=" + min);
        }
        if(max != null ){
            stringBuilder.append("&max=" + max);
        }
        return stringBuilder.toString();
    }

}
