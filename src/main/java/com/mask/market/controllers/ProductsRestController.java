package com.mask.market.controllers;

import com.mask.market.entities.Product;
import com.mask.market.exceptions.NotFoundResourceException;
import com.mask.market.services.ProductsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/products")
@RequiredArgsConstructor
public class ProductsRestController {

    private final ProductsService productsService;

    @GetMapping
    public List<Product> getProducts(){

//        new ProductsController()

        return productsService.findAllProducts();

    }

    @RequestMapping
    @GetMapping("/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable Long id){
        Product productById = productsService.findProductById(id);
        ResponseEntity<Product> responseEntity = new ResponseEntity<>(productById, HttpStatus.OK);

        return responseEntity;
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Product addNewProduct(@RequestBody Product product){

        if(product.getId() == null){
            return productsService.saveProduct(product);
        }

        throw new RuntimeException();
    }

    @PutMapping
    public Product changeProduct(@RequestBody Product product){

        Long id = product.getId();
        if(id != null){
            if (productsService.existProductById(id)) {
                return productsService.editProduct(product);
            }
        }
         throw new NotFoundResourceException();
    }

}
