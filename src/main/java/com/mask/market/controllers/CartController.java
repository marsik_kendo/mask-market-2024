package com.mask.market.controllers;

import com.mask.market.entities.Cart;
import com.mask.market.services.CartService;
import jakarta.persistence.EntityManager;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Base64;

@Controller
@RequestMapping("/cart")
@RequiredArgsConstructor
public class CartController {

    @Autowired(required = false)
    private CartService cartService;

    @Autowired
    private EntityManager entityManager;

    @GetMapping
    private String showCart(Model model,
                            @CookieValue(name = "cartId",required = false) Long cookiesCartId,
                            HttpServletResponse response){

        Cart cart;
        cart = cartService.findCart(cookiesCartId, response);


        model.addAttribute("items", cart.getItems());
        model.addAttribute("price", cart.getPrice());
        model.addAttribute("cartId",  Base64.getEncoder().encodeToString(cart.getId().toString().getBytes()));
        return "cart";
    }
    @GetMapping("/{id}")
    private String showCartById(Model model,@PathVariable Long id){

        Cart cart = cartService.findCartById(id);
        boolean contains = entityManager.contains(cart);
        System.out.println("qwe2 = "+contains);

        model.addAttribute("cart", cart);
        model.addAttribute("items", cart.getItems());
        model.addAttribute("price", cart.getPrice());
        model.addAttribute("cartId",  cart.getId());
        return "cart";
    }



    @GetMapping("/addProduct/{productId}")
    public String addProductToCart(@PathVariable Long productId,
                                   @CookieValue(name = "cartId",required = false) Long cartId,
                                   HttpServletResponse response){

        cartService.addProduct(productId, cartId, response);

        return "redirect:/shop";
    }




}
