package com.mask.market.controllers;

import com.mask.market.entities.Order;
import com.mask.market.services.CartService;
import com.mask.market.services.OrderService;
import com.mask.market.services.UserService;
import jakarta.servlet.http.Cookie;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.extras.springsecurity6.util.SpringSecurityContextUtils;

import java.security.Principal;
import java.util.Base64;

@RequiredArgsConstructor
@Controller
@RequestMapping("/orders")
public class OrderController {


    private final OrderService orderService;

    @GetMapping("/prepared/{cartId}")
    public String preparedOrder(@PathVariable String cartId, Model model){


        String s = new String(Base64.getDecoder().decode(cartId.getBytes()));
        model.addAttribute("cartId", s);
        return "order_confirm";

    }

    @GetMapping("/create")
    public String createOrder(Principal principal){

        orderService.createOrder(principal.getName());
        return "redirect:/shop";
    }

}
