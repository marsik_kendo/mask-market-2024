package com.mask.market.controllers;

import com.mask.market.entities.Product;
import com.mask.market.entities.User;
import com.mask.market.services.ProductsService;
import com.mask.market.services.UserService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpRequest;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

@Controller
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductsController {
//
//    private  final ProductsService productsService;
//
//    private final UserService userService;
//
//
//    @GetMapping
//    public String showProducts(Model model,
//                               HttpServletRequest request,
//                               @RequestParam(required = false) String word,
//                               @RequestParam(required = false) Integer min,
//                               @RequestParam(required = false) Integer max,
//                               @RequestParam(required = false, defaultValue = "1") Integer pageNumber){
//
//        System.out.println(userService);
//        System.out.println(productsService);
//        PageRequest pageRequest = PageRequest.of(pageNumber - 1, 1, Sort.Direction.DESC, "title");
//        Page<Product> page = productsService.findAllProducts(word, min, max , pageRequest);
//        String filters = getFilters(word, min, max);
//
//
//
//
//        model.addAttribute("page", page);
//        model.addAttribute("filters", filters);
//
//        return "products";
//    }
//
//    private String getFilters(String word, Integer min, Integer max){
//
//        StringBuilder stringBuilder = new StringBuilder();
//        if(word != null && !word.isEmpty()){
//            stringBuilder.append("&word=" + word);
//        }
//        if(min != null ){
//            stringBuilder.append("&min=" + min);
//        }
//        if(max != null ){
//            stringBuilder.append("&max=" + max);
//        }
//        return stringBuilder.toString();
//    }
//
//
//    @GetMapping("/delete/{id}")
//    public String deleteProduct(@PathVariable Long id){
//
//        productsService.deleteProductById(id);
//        return "redirect:/products";
//    }
//
//    @GetMapping("/changeProduct/{id}")
//    public String changeProduct(@PathVariable Long id, Model model){
//
//        Product productById = productsService.findProductById(id);
//
//        model.addAttribute("product", productById);
//        return "edit_product";
//    }
//
//    @PostMapping("/executeChangeProduct")
//    public String changeProduct(Product product, @RequestParam Long id){
//
//        productsService.saveProduct(product);
//        return "redirect:/products";
//    }




}
