package com.mask.market.controllers.adviced;

import com.mask.market.exceptions.NotFoundResourceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class AdviceController {


    @ExceptionHandler({NotFoundResourceException.class})
    public ResponseEntity<ResponseError> handleResourceException(){

        ResponseError responseError = new ResponseError();
        responseError.setCreateAt(LocalDateTime.now());
        responseError.setMessage("Такого ресурса не нашлось");
        responseError.setStatusCode(404);

        return new ResponseEntity<>(responseError, HttpStatus.OK);
    }
}
