package com.mask.market.controllers.adviced;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class ResponseError {

    private String message;

    private LocalDateTime createAt;

    private int statusCode;
}
