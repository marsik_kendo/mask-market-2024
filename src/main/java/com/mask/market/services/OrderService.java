package com.mask.market.services;


import com.mask.market.entities.Cart;
import com.mask.market.entities.Order;
import com.mask.market.entities.User;
import com.mask.market.repositories.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;

    private final UserService userService;

    private final CartService cartService;

    private final MailService mailService;


    public Order createOrder(String username) {

        User user = userService.getUserByUserName(username);
        Cart cart = user.getCart();
        Order order = new Order(user, cart);
        order = orderRepository.save(order);
        cartService.cleanCart(cart);
        mailService.sendOrderMail(order);
        return order;
    }
}
