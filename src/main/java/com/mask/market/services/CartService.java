package com.mask.market.services;

import com.mask.market.entities.Cart;
import com.mask.market.entities.CartItem;
import com.mask.market.entities.Product;
import com.mask.market.entities.User;
import com.mask.market.repositories.CartItemsRepository;
import com.mask.market.repositories.CartRepository;
import com.mask.market.repositories.UserRepository;
import jakarta.annotation.PostConstruct;
import jakarta.persistence.EntityManager;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service

@RequiredArgsConstructor
public class CartService {

    private final UserService userService;

    private final CartRepository cartRepository;

    private final CartItemsRepository cartItemsRepository;

    private final ProductsService productsService;

    private final CartItemsService cartItemsService;

    private final UserRepository userRepository;

    @Autowired
    private EntityManager entityManager;

    @Value("${path.to.domain}")
    private String contextPath;


    private void addProduct(Long productId, Cart cart) {


        CartItem cartItem = cart.getItems()
                .stream()
                .filter(item -> item.getProduct().getId().equals(productId))
                .findFirst().orElse(null);

        if(cartItem != null ){
            cartItem.changeQuantity(true);
            cartItemsRepository.save(cartItem);
        } else {
            Product product = productsService.findProductById(productId);
            cartItem = new CartItem(product, cart);
            cart.getItems().add(cartItem);
        }
        cart.recalculatePrice();
        cartRepository.save(cart);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void cleanCart(Cart cart) {
        
        for (CartItem cartItem : cart.getItems()) {
            cartItemsService.removeItem(cartItem);
        }
        cart.setItems(new ArrayList<>());
        cart.setPrice(0);
        cartRepository.save(cart);
    }


    public void addProduct(Long productId, Long cartId, HttpServletResponse response) {

        Cart cart = findCart(cartId, response);
        addProduct(productId, cart);
    }

    public Cart findCart(Long cookieCartId, HttpServletResponse response) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Cart cart;
        if(authentication instanceof AnonymousAuthenticationToken){ // проверка на то что пользователь не аутентифицирован
            cart = cookieCartId != null ?
                    cartRepository.findById(cookieCartId).orElseThrow() : cartRepository.save(new Cart());
        } else {
            User user = userService.getUserByUserName(authentication.getName());
            cart = user.getCart();
            if (cart == null) {
                Cart newCartForUser = cartRepository.save(new Cart());
                cart = newCartForUser;
                user.setCart(newCartForUser);
                if (cookieCartId != null) {
                    if ( !userRepository.existsByCartId(cookieCartId) ) {
                        shiftItems(cartRepository.findById(cookieCartId).orElseThrow(), cart);
                    }
                }
                userRepository.save(user);
            } else {
                if (cookieCartId != null) {
                    if (!userRepository.existsByCartId(cookieCartId) && !Objects.equals(cookieCartId, cart.getId())) {
                        shiftItems(cartRepository.findById(cookieCartId).orElseThrow(), cart);
                    }
                }
            }
        }
        cart = cartRepository.save(cart);
        addCookiesToResponse(response, cart.getId());
        return cart;
    }

    private void shiftItems(Cart source, Cart destination){
        source.getItems().forEach(cartItem -> {
            cartItem.setCart(destination);
            cartItemsRepository.save(cartItem);
        });
        source.setPrice(0);
        cartRepository.save(source);
        //todo надо проверить
//        cartRepository.save(destination);
    }

    private void addCookiesToResponse(HttpServletResponse response, Long cartId) {
        Cookie cookie = new Cookie("cartId", cartId.toString());
        cookie.setPath(contextPath);
        response.addCookie(cookie);
    }

    @Transactional(readOnly = true)
    public Cart findCartById(Long id) {

        Cart cart = cartRepository.findById(id).orElse(null);
//        Cart save = cartRepository.save(new Cart());
//        System.out.println(save.getId());
        System.out.println(cart.getItems().size());
        System.out.println("qwe1 = "+entityManager.contains(cart));
        return cart;
    }
}
