package com.mask.market.services;


import com.mask.market.entities.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Component
@RequiredArgsConstructor
public class MailMessageBuilder {

    private final TemplateEngine templateEngine;


    public String buildOrderEmail(Order order) {
        Context context = new Context();
        context.setVariable("order", order);
        return templateEngine.process("order_mail", context);
    }


//    public String buildPassEmail(String newPass){
//        Context context = new Context();
//        context.setVariable("password", newPass);
//        return  templateEngine.process("user_mail", context);
//
//    }
}
