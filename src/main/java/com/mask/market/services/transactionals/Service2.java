package com.mask.market.services.transactionals;


import com.mask.market.entities.Cart;
import com.mask.market.entities.User;
import com.mask.market.repositories.CartRepository;
import com.mask.market.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class Service2 {
    @Autowired
    private UserRepository userRepository;




    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.REPEATABLE_READ)
    public void qwe(){
        User admin1 = userRepository.save(new User("admin1"));
        System.out.println(admin1.getId());

    }
}
