package com.mask.market.services.transactionals;

import com.mask.market.entities.User;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Service1Proxy {



    private final EntityManager entityManager;

    private final Service1 service1;


    public void qwe() {
        entityManager.getTransaction().begin();

        service1.qwe();

        entityManager.getTransaction().commit();
    }

    public void qwe1() {
        entityManager.getTransaction().begin();

//        service1.qwe1();

        entityManager.getTransaction().commit();
    }
}
