package com.mask.market.services;

import ch.qos.logback.classic.spi.EventArgUtil;
import com.mask.market.entities.Product;
import com.mask.market.exceptions.NotFoundResourceException;
import com.mask.market.mappers.ProductMapper;
import com.mask.market.repositories.ProductsRepository;
import com.mask.market.repositories.ProductsSpecifications;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.ServerSocket;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

@Service
@RequiredArgsConstructor
public class ProductsService {


    private final ProductsRepository productsRepository;

    private final ProductMapper productMapper;

    private final EntityManager entityManager;

    public List<Product> findAllProducts(){

       return (List<Product>) productsRepository.findAll();
    }
    public Page<Product> findAllProducts(String word, Integer min, Integer max, Pageable pageable){

        LocalDate now = LocalDate.now();
        // 15.03.2024 00:00:00'00000


        Specification<Product> spec = Specification.where(null);
        if(word !=null){
            spec =  spec.and(ProductsSpecifications.titleLike(word));
        }
        if(min !=null){
            spec =   spec.and(ProductsSpecifications.priceGreaterThan(min));
        }
        if(max !=null){
            spec =  spec.and(ProductsSpecifications.priceLessThan(max));
        }
        return productsRepository.findAll(spec, pageable);
    }

    public void deleteProductById(Long id){


        productsRepository.deleteById(id);

    }

    public Product findProductById(Long id) {

       return productsRepository.findById(id).orElseThrow(NotFoundResourceException::new);
    }


    public Product saveProduct(Product product) {

      return   productsRepository.save(product);
    }

    public Product editProduct(Product product) {

        return   productsRepository.save(product);
    }
    public boolean existProductById(long id) {
       return productsRepository.existsById(id);

    }
}
