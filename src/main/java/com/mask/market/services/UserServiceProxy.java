package com.mask.market.services;

import com.mask.market.entities.User;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceProxy  {

    private final UserService userService;

    private final EntityManager entityManager;


    public User getUserByUserName(String name) {
        entityManager.getTransaction().begin();

        User userByUserName = userService.getUserByUserName(name);

        entityManager.getTransaction().commit();
        return userByUserName;
    }


}
