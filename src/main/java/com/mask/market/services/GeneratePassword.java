package com.mask.market.services;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class GeneratePassword {

    private static final String upper= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private static final String lower = upper.toLowerCase();

    private static final String digits = "0123456789";

    private  static final String alphanum = upper + lower + digits;

    private static final int sizePass=12;

    public String generate(){
        Random random = new Random();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i <sizePass ; i++) {
            int randomIndex  = random.nextInt(62);
            result.append(alphanum.charAt(randomIndex));
        }
        return result.toString();
    }
}
