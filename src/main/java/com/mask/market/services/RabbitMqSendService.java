package com.mask.market.services;

import java.io.Serializable;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mask.market.model.Cat;
import com.mask.market.rabitmq.RabbitReceiver;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

//@Slf4j
@Service
@RequiredArgsConstructor
public class RabbitMqSendService {

    private final RabbitTemplate rabbitTemplate;

    private final ObjectMapper objectMapper;

    @PostConstruct
    public void init() throws Exception{

        Cat cat = new Cat();
        cat.setAge(10);
        cat.setName("Ivan");
        cat.setColor("green");
        sendOutObj(cat, "zxc1");
    }

    public void sendOutObj(Cat cat, String routingKey) throws JsonProcessingException {

        MessageProperties properties = new MessageProperties();
        properties.setHeader("DOT_MSG_TYPE", "qwe123");
        //        properties.setHeader(DOT_BANK_CODE, bankCode);
        //        properties.setHeader(RabbitConfiguration.DOT_MSG_FROM, esbEvent.getSender());
        //        properties.setHeader(RabbitConfiguration.DOT_EVENT_GUID, esbEvent.getRefer());
        //        properties.setHeader(RabbitConfiguration.DOT_EXT_KEY, esbEvent.getExtkey());
        properties.setContentType(MessageProperties.CONTENT_TYPE_TEXT_PLAIN);

        //куда то на выход, но можно пока подложить входящий эксченч
        rabbitTemplate.send("out.exchange", routingKey,
                new Message(objectMapper.writeValueAsBytes(cat), properties));

    }

}
