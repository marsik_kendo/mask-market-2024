package com.mask.market.services;


import com.mask.market.entities.CartItem;
import com.mask.market.repositories.CartItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CartItemsService {


   private CartItemsRepository cartItemsRepository;

    @Autowired
    public void setCartItemsRepository(CartItemsRepository cartItemsRepository) {
        this.cartItemsRepository = cartItemsRepository;
    }

    public void removeItem(CartItem item){
        cartItemsRepository.deleteById(item.getId());
    }

}
