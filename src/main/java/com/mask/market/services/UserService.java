package com.mask.market.services;


import com.mask.market.entities.Role;
import com.mask.market.entities.User;
import com.mask.market.repositories.UserRepository;
import com.mask.market.utils.SystemUser;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;


    private final BCryptPasswordEncoder encoder;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findOneByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("Invalid login or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                mapRolesToAuthorities(user.getRoles()));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
        return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName())).toList();
    }



    @Transactional(readOnly = true)
    public User getUserByUserName(String name) {
        // tx properties - ACID
        // Atomicity
        // consistency
        // isolation
        // Durability
        // tx begin
        User oneByUsername = userRepository.findOneByUsername(name);
        System.out.println(oneByUsername.getRoles());

        return oneByUsername;
        // tx commit;
    }

    public void updateUser(User user) {

        userRepository.save(user);
    }

    public void save(SystemUser systemUser) {

        User user = new User();
        user.setPassword(encoder.encode(systemUser.getPassword()));
        user.setUsername(systemUser.getUsername());
        user.setFirstName(systemUser.getFirstName());
        user.setEmail(systemUser.getEmail());
        user.setLastName(systemUser.getLastName());
        user.setPhone("1234543");
        userRepository.save(user);
    }
}
