package com.mask.market.mappers;


import com.mask.market.entities.Product;
import com.mask.market.model.dto.ProductDtoImpl;
import org.mapstruct.Mapper;

@Mapper
public interface ProductMapper {

    ProductDtoImpl entityToDto(Product product);


    Product dtoToEntity(ProductDtoImpl productDto);

}
