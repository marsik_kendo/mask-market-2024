package com.mask.market.configs;


import com.mask.market.controllers.MainController;
import com.mask.market.entities.User;
import com.mask.market.services.UserService;
import org.apache.catalina.filters.RemoteAddrFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AuthorizeHttpRequestsConfigurer;
import org.springframework.security.config.annotation.web.configurers.FormLoginConfigurer;
import org.springframework.security.config.annotation.web.configurers.LogoutConfigurer;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity(securedEnabled = true)
public class SecurityConfig {


    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider(UserService userService,
                                                               PasswordEncoder passwordEncoder) {

        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userService);
        provider.setPasswordEncoder(passwordEncoder);
        return provider;
    }


//    @Bean
//    public RemoteAddrFilter addrFilter(){
//        RemoteAddrFilter remoteAddrFilter = new RemoteAddrFilter();
////        remoteAddrFilter.setAllow("127.0.0.1");
////        remoteAddrFilter.setDeny("");
//        return remoteAddrFilter;
//
//    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http, MainController controller) throws Exception {



        http.authorizeHttpRequests(authorizationManagerRequestMatcherRegistry -> authorizationManagerRequestMatcherRegistry
                .requestMatchers("/admin/users/**").hasRole("ADMIN")
                .requestMatchers("/admin/**").hasAnyRole("ADMIN", "MANAGER")
                .requestMatchers("/cart/**").permitAll()
                .requestMatchers("/orders/**").authenticated()
                .requestMatchers("/personal/**").permitAll()
                .anyRequest().permitAll())

                .formLogin(form -> form
                        .loginPage("/login")
                        .defaultSuccessUrl("/shop")
                        .loginProcessingUrl("/authenticateTheUser")
                        .permitAll())
                .logout(form -> {
                    form.deleteCookies("JSESSIONID");
                    form.logoutSuccessUrl("/shop");
                })
//                .httpBasic(Customizer.withDefaults())
                ;

        return http.build();
    }
}