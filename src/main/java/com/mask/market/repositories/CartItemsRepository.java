package com.mask.market.repositories;

import com.mask.market.entities.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CartItemsRepository extends JpaRepository<CartItem, Long> {


    @Query(value = "select * from cart_items ci left join products p on cart_items.product_id = products.id", nativeQuery = true)
    List<CartItem> findAllCartItems();
}
