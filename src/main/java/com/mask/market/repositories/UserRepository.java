package com.mask.market.repositories;

import com.mask.market.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface UserRepository extends CrudRepository<User, Long> {

    User findOneByUsername(String username);

    // select exists(select 1 from users where cartId = :cartId)
    boolean existsByCartId(Long cartId);
}
