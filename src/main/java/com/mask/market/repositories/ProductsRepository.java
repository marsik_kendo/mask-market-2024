package com.mask.market.repositories;


import com.mask.market.entities.Product;
import com.mask.market.model.dto.ProductDto;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

public interface ProductsRepository extends CrudRepository<Product, Long>,
        JpaSpecificationExecutor<Product>, PagingAndSortingRepository<Product, Long> {


    @Query(value = "select * from products", nativeQuery = true)
    List<ProductDto> qwe();

//    @Modifying
//    @Transactional
//    @Query(value = "insert into products values(:id, :title, :price)", nativeQuery = true)
//    int saveProduct(long id, String title, int price);
}
