package com.mask.market.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Entity
@Table(name = "carts")
@Getter
@Setter
public class Cart implements Serializable {


    private static final long serialVersionUID = 8497525498315276562L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @OneToMany(mappedBy = "cart", cascade = CascadeType.ALL)
    private List<CartItem> items;

    @Column
    private int price;

    public Cart() {
        this.items = new ArrayList<>();
        this.price = 0;
    }


    public void recalculatePrice() {
        price = 0;
        items.forEach(cartItem -> price += cartItem.getTotalPrice());
    }
}
