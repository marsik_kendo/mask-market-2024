package com.mask.market.entities;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import jakarta.persistence.*;
import lombok.Setter;


@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "order_items")
public class OrderItem {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne()
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "item_price")
    private int itemPrice;

    @Column(name = "total_price")
    private int totalPrice;

    public OrderItem(CartItem item) {
        this.product = item.getProduct();
        this.itemPrice = item.getItemPrice();
        quantity = item.getQuantity();
        this.totalPrice = quantity * itemPrice;
    }
}
