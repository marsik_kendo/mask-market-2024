package com.mask.market.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Entity
@Table(name = "cart_items")
@Getter
@Setter
@NoArgsConstructor
public class CartItem implements Serializable {

    private static final long serialVersionUID = 1276801961638051142L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "cart_id")
    private Cart cart;

    @Column
    private int quantity = 1;

    @Column(name = "item_price")
    private int itemPrice;


    @Column(name = "total_price")
    private int totalPrice;


    public CartItem(Product product, Cart cart) {
        this.product = product;
        this.cart = cart;
        this.itemPrice = product.getPrice();
        this.totalPrice = itemPrice;
    }



    public void changeQuantity(boolean side){
        if (side) {
            quantity++;
        } else {
            quantity--;
        }

        recalculate();
    }


    private void recalculate(){

        totalPrice = product.getPrice() * quantity;

    }

}
