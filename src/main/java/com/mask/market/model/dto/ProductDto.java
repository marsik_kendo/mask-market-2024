package com.mask.market.model.dto;

public interface ProductDto {

    Long getId();

    String getTitle();

    int getPrice();
}
