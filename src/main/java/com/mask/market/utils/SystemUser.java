package com.mask.market.utils;



import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.validation.constraints.*;


@Data
@NoArgsConstructor
@FieldMatch(first = "password", second = "matchingPassword", message = "The password fields must match")
public class SystemUser {

    @NotNull(message = "is required")
    @Size(min = 4, message = "username is required")
    private String username;

    @NotNull(message = "is required")
    @Size(min = 4, message = "email is required")
    @Email
    private String email;

    @NotNull(message = "is required")
    @Size(min = 3, message = "слишком короткий пароль")
    private String password;

    @NotNull(message = "is required")
    @Size(min = 3, message = "пароли не совпадают")
    private String matchingPassword;

    @NotNull(message = "is required")
    @Size(min = 1, message = "firstName is required")
    private String firstName;

    @NotNull(message = "is required")
    @Size(min = 1, message = "lastName is required")
    private String lastName;



}
