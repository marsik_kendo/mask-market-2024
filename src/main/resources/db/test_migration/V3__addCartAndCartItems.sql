drop table if exists carts;

create table carts (
    id bigserial primary key,
    price int not null default 0
);

drop table if exists cart_items;
create table cart_items (
    id bigserial,
    product_id bigint not null,
    cart_id bigint not null,
    quantity int not null,
    item_price int,
    total_price int,
    primary key (id),
    constraint fk_cart_items_on_cart foreign key (cart_id) references carts(id),

    constraint fk_cart_items_on_product foreign key (product_id) references products(id)

);


alter table users add column cart_id bigint;

ALTER TABLE users
    ADD CONSTRAINT fk_users_carts FOREIGN KEY (cart_id)
        REFERENCES carts (id);