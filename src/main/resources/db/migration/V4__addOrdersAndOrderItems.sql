
DROP TABLE IF EXISTS orders;
CREATE TABLE orders (
                        id                  bigserial,
                        user_id             bigint NOT NULL,
                        price               int,
                        created_at          TIMESTAMP,
                        updated_at          TIMESTAMP,

                        PRIMARY KEY (id),
                        CONSTRAINT FK_USER_ID_IN_ORDERS FOREIGN KEY (user_id)
                            REFERENCES users (id)
                            on update cascade
                            on delete cascade
);


DROP TABLE IF EXISTS order_items;
CREATE TABLE order_items(
                            id                  bigserial,
                            product_id          bigint NOT NULL,
                            order_id            bigint NOT NULL,
                            quantity               int NOT NULL,
                            item_price          int NOT NULL,
                            total_price         int NOT NULL,
                            PRIMARY key (id),
                            CONSTRAINT FK_ORDER_ID_IN_ORDER_ITEMS FOREIGN KEY (order_id)
                                REFERENCES orders(id)
                                on update cascade
                                on delete cascade ,

                            CONSTRAINT FK_PRODUCT_ID_IN_ORDER_ITEMS FOREIGN KEY (product_id)
                                REFERENCES products(id)
);