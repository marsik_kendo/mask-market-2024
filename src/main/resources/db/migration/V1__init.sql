drop table if exists products;
create table products(
    id bigserial primary key ,
    title varchar(255),
    price int
);
insert into  products (title, price) values
('Сыр', 120),
('Молоко', 90),
('Хлеб', 300),
('Колбаса', 120),
('Масло', 90),
('Кока-кола', 300),
('Пепси', 300),
('Яблоко', 90),
('Лимонад', 300);






